from dataclasses import dataclass
from typing import Any, Callable, List

import torch

from .samples import Sample


class Batch:
    def __init__(self, samples: List[Sample] = None, return_tensors: str = None, device: str = None, **kwargs):
        self.samples = samples
        self.return_tensors = return_tensors
        self.device = device

    def attr_get(self, key: str):
        out = [getattr(s, key) for s in self.samples]
        if self.return_tensors == "pt":
            out = torch.cat(out).to(self.device)
        return out

    @property
    def data(self):

        out = {}  # use this instead of defaultdict(list) because it looks better in debugger
        sample: Sample
        for sample in self.samples:
            if sample.data.data_type not in out:
                out[sample.data.data_type] = []
            out[sample.data.data_type].append(sample.data)

        if self.return_tensors == "pt":
            for key, val in out.items():
                out[key] = torch.cat(val).to(self.device)

        return out

    @property
    def target(self):
        # return self.attr_get("target")
        out = [getattr(s, "target") for s in self.samples]
        if self.return_tensors == "pt":
            if out[0].ndim == 1:
                out = torch.stack(out).to(self.device)
            else:
                out = torch.cat(out).to(self.device)
        return out

    # TODO: refactor this so it is similar to attr_get
    def get_masks(self, key: str):
        out = [s.masks[key] for s in self.samples]
        if self.return_tensors == "pt":
            if out[0].ndim == 1:
                out = torch.stack(out).to(self.device)
            else:
                out = torch.cat(out).to(self.device)

        return out

    @property
    def tasks(self):
        return [s.task_type for s in self.samples]

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, key: int):
        return self.samples[key]

    def __iter__(self):
        return iter(self.samples)

    @classmethod
    def collate_fn(cls, samples: List[Sample]) -> "Batch":
        """collate_fn for torch.utils.data.DataLoader"""
        batch = cls(samples)
        return batch


@dataclass
class DataHandlerPath:
    module: Callable[...]
    name: str = None  # the name to bind to the handler object
    data_type: str = None  # the data type it handles

    def __post_init__(self):
        if self.name is None:
            self.name = self.module.__class__.__name__
        if self.data_type is None:
            self.data_type = self.module.data_type

from enum import Enum
from typing import Any, Callable, Dict, List

import torch
import torch.nn as nn

from .input_types import InputType


class SampleMetaData:
    idx: Any = None
    dataset_name: Any = None

    def __init__(self, **kwargs):
        for key, val in kwargs.items():
            setattr(self, key, val)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.__dict__})"

    @classmethod
    def make(cls, idx: int | slice, **kwargs):
        if isinstance(idx, slice):
            raise NotImplementedError("metadata for slices not implemented")
        metadata = cls(idx=idx, **kwargs)
        return metadata


class Sample:
    def __init__(
        self,
        data: List[InputType] = None,
        target: Any = None,
        masks: Dict[str, Any] = {},
        metadata: SampleMetaData = None,
        task_type: str = None,
    ) -> None:
        self.data = data
        self.target = target
        self.metadata = metadata
        self.masks = masks

        # task type helps with how target should be used
        self.task_type = task_type

    def __call__(self, **kwargs):
        for key, val in kwargs.items():
            setattr(self, key, val)

    def __iter__(self):
        yield self._data, self._target

    def __repr__(self) -> str:
        # added this so its easier to look at during debugging
        data_str = f"data(shape)[{self.data.shape}]" if isinstance(self.data, torch.Tensor) else f"data={self.data}"
        target_str = f"target(shape)[{self.target.shape}]" if isinstance(self.target, torch.Tensor) else f"{self.target}"

        string = f"Sample(data={data_str}, target={target_str}"
        if self.masks:
            mask_str = ", ".join([f"{k}={v.shape}" for k, v in self.masks.items()])
            string += f", masks={mask_str}"

        if self.metadata is not None:
            string += f", metadata={self.metadata}"
        string += ")"
        return string

    def _input_type_hook(self, hook_fn: Callable):
        hook_fn(self)

    @classmethod
    def new(cls, **kwargs):
        return cls(**kwargs)


class SampleBuilder:
    metadata: SampleMetaData = SampleMetaData

    def __init__(self, *args, **kwargs):
        self.task_type = None
        self.preprocessing: List[Callable] = []

    def __call__(self, *args, **kwargs):
        key: str
        val: Dict[str, Any]

        sample = Sample(*args, **kwargs)

        if self.task_type:
            sample.task_type = self.task_type

        for func in self.preprocessing:
            if isinstance(func, str):
                func = getattr(sample.data, func)(sample)

            else:
                func(sample)

        return sample

    def with_task_type(self, task_type: str | Enum):
        self.task_type = task_type.task_type

    def use_preprocessing(self, func: Callable):
        self.preprocessing.append(func)


class SampleBuilderMixin:
    sample_builder = SampleBuilder()
